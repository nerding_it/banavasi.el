;;; banavasi.el --- Banavasi mode a major mode for editing using Kannada

;; Author: nerding_it <virtualxi99@gmail.com>
;; Created:  5 Jul 2021
;; Keywords: Kannada, Banavasi, Emacs

;;; Commentary: 

;; A major mode for editing using kannada using Emacs

;;; Code:

(define-derived-mode banavasi-mode fundamental-mode "ಕನ್ನಡ ಇಮಾಕ್ಸ್"
  "Major mode for editing files using Kannada.")

(defun banavasi/activate-input-method ()
  "Activate input method kannada."
  (activate-input-method "kannada-itrans"))

(advice-add 'banavasi-mode :before #'banavasi/activate-input-method)

(add-to-list 'auto-mode-alist '("\\.bn\\'" . banavasi-mode))

(provide 'banavasi)

;;; banavasi.el ends here
